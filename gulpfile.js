var elixir = require('laravel-elixir');


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir.config.sourcemaps = false;

elixir(function(mix) {
    mix.rubySass(['auth.sass'], 'public/assets/css/auth.css');
    mix.rubySass(['dashboard.sass'], 'public/assets/css/dashboard.css');
    mix.sass(['lorrein.scss'], 'public/assets/css/lorrein.css');
});