# Daley dashboard

This dashboard will enable the supervisors of the mentally challenged to control their clients and schedule tasks for them.

## Daley routes


## Test location

Still under construction

## Installation
### Eerste keer clonen
1. Open je terminal
2. Navigeer in je terminal naar je project doormiddel van de command "cd /path/to/folder"
3. Type "git clone http://git.ma-dev.nl/Jurgen/daley.git" en vul je ma-dev gegevens in
4. Zodra het clonen klaar is, navigeer je naar je project folder in je terminal
5. Type in "php composer.phar install" en wacht tot het klaar is met downloaden
6. Type in "cp .env.example .env" en daarna "php artisan key:generate"
7. Type in "git checkout NAAM"

### Het project starten
Zodra alles klaar is, kun je "php artisan serve" intypen en dan zou de server moeten starten op "localhost:8000"

### Om project files op te halen
Type in "git pull" of "git pull origin NAAM"

### Om project files op te slaan
Type in "git push" of "git push origin NAAM"

## Bestand locaties
### CSS
public/assets/css/

### Javascript
public/assets/js/

### Views
resources/views/pages/
resources/views/layouts/

Please follow the guide at [laravel.com](https://laravel.com)

## API info

All the api info can be found on [this page](http://ma-dev.nl/Jurgen/daley/wikis/api-info)
