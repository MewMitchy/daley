<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* Collection of all routes */
Route::controllers([
	'auth' 		=> 'Auth\AuthController',
	'password' 	=> 'Auth\PasswordController',
	'client'	=> 'ClientController',
	'user'		=> 'UserController',
	'task'		=> 'TaskController'
]);

Route::get('/', 'DashboardController@getIndex');

Route::get('/language/{language}', 'LanguageController@index');

Route::get('/insert/task', 'InsertController@addTask');

// Route::any('/{slug}', function($slug){
// 	if(!session('auth_token')){
//     	return redirect('/auth/login')->with('error', ['This page does not exist!']);
// 	}
// 	else{
//     	return redirect('/')->with('error', ['This page does not exist!']);
// 	}
// })->where('slug', '([A-z\d-\/_.]+)?');
