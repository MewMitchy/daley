<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use GuzzleHttp;
use Route;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class NavigationComposer
{

	public function compose(View $view)
	{
            $guzzle_client = new Client;

            $url = 'http://bemika.webfactional.com';

            $user_credentials = [
                'headers' => [
                    'Authorization' => 'Token ' . session('auth_token')
                ]
            ];

            // Call to user info
            $user_info_response = $guzzle_client->get($url.'/rest-api/get_user/', $user_credentials);

            // Get user info
            $user_info = json_decode($user_info_response->getBody()->getContents());

            $view->with('user_info', $user_info);
	}
}
