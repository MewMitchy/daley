<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Request;
use App\Http\Requests;

use GuzzleHttp;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Response;
use App;


class Controller extends BaseController
{
    // use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /*
    * Public variables
    * Use in controller: $this->variable
    */
    public $guzzle_client;
    public $url;
    public $user_credentials;


	public function __construct(Client $guzzle_client){
        // Check if the user is logged in, else send back to login page
        if(!session('auth_token')){
            return redirect('/auth/login')->send();
        }

        // public variable values
        $this->guzzle_client = $guzzle_client;
        $this->url = 'http://bemika.webfactional.com';

        // user credentials for api
        $this->user_credentials = [
            'headers' => [
                'Authorization' => 'Token ' . session('auth_token')
            ]
        ];

        // Call to user info
        $user_info_response = $this->guzzle_client->get($this->url.'/rest-api/get_user/', $this->user_credentials);
        // Get user information
        $user = json_decode($user_info_response->getBody()->getContents());

        if(session()->has('locale')){
            App::setLocale(session()->get('locale'));
        }
        else{
            App::setLocale($user->data->language);
        }

    }
    
}
