<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class DashboardController extends Controller
{
    /*
    * User login to dashboard show this function
    * Get Username and the client list
    */
    public function getIndex(){
        // Call to user client info
        $user_client_response = $this->guzzle_client->get($this->url.'/rest-api/get_patient_list/', $this->user_credentials);

        // Get user client list
        $client_list = json_decode($user_client_response->getBody()->getContents());

        // Limited to 10 for testing
        $client_list->data = array_slice($client_list->data, 0, 10);

        // if client_list has one value return that view
        if(count($client_list->data) === 1){
            return redirect('/client/show/'.$client_list->data[0]->id);
        }elseif (empty($client_list->data)) {
            // if client_list is empty than return to client/create view
            return view('pages.client.create');
        }elseif($client_list->data >= 1){
            return redirect('/client/show/'.$client_list->data[4]->id);
            
            $this_client = [];

            foreach($client_list->data as $client){
                 // Call to user info
                $this_client_info_response = $this->guzzle_client->get($this->url.'/rest-api/get_patient/'.$client->id, $this->user_credentials);

                // Get user client list
                $this_client_info = json_decode($this_client_info_response->getBody()->getContents());

                array_push($this_client, ['id' => $this_client_info->data->id, 'name' => $this_client_info->data->name, 'color' => $this_client_info->data->color_identifier]);
            }

            // if client_list has more than one value than return the calendar 
            return view('pages.dashboard', compact('client_list', 'this_client'));
        }

    }


}
