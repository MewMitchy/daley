<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use JavaScript;

use Carbon\Carbon;


class ClientController extends Controller
{
    /*
    * Show single target client
    * use $id to get the single client
    */
    public function getShow($id){
        // Call to user info
        $client_info_response = $this->guzzle_client->get($this->url.'/rest-api/get_patient/'.$id, $this->user_credentials);

        // Get user client list
        $client_info = json_decode($client_info_response->getBody()->getContents());

        // All tasks
        $task_list = $client_info->data->task_set;

        // Define today
        $today = Carbon::today()->format('Y-m-d');
        // $today = '2016-04-26';

        // Empty today tasks
        $today_tasks = [];

        // Loop through tasks
        foreach($task_list as $task){
            // Typecast object to array
            $task = (array)$task;

            // Check if in array, then push to today_tasks
            if($task['start_date'] == $today) {
                $today_tasks[] = (object)$task;
            }
        }

        // Sort tasks by time
        usort($today_tasks, function($a, $b) {
            if ($b->start_time == "00:00:00") return false;
            return $a->start_time - $b->start_time;
        });

        $dates = [];

        foreach($task_list as $task){
            $date = (array)$task;
            $date = $date['start_date'];

            array_push($dates, $date);
        }



        // Call to user client list info
        $user_client_response = $this->guzzle_client->get($this->url.'/rest-api/get_patient_list/', $this->user_credentials);
        // Get user client list
        $client_list = json_decode($user_client_response->getBody()->getContents());
        // $client_list->data = array_slice($client_list->data, 0, 10);

        // Removing clients
        $filter_keys = array_flip([4, 5, 6, 7, 8]);
        $client_list->data = array_intersect_key($client_list->data, $filter_keys);

        $this_client = [];

        foreach($client_list->data as $client){
             // Call to user info
            $this_client_info_response = $this->guzzle_client->get($this->url.'/rest-api/get_patient/'.$client->id, $this->user_credentials);

            // Get user client list
            $this_client_info = json_decode($this_client_info_response->getBody()->getContents());

            array_push($this_client, ['id' => $this_client_info->data->id, 'name' => $this_client_info->data->name, 'color' => $this_client_info->data->color_identifier]);
        }

        JavaScript::put([
            'client_id' => $id,
            '_token' => csrf_token(),
            'taken' => $task_list,
            'task_count' => array_count_values(array_replace($dates,array_fill_keys(array_keys($dates, null),'')))
        ]);
        // Dit wordt automatisch naar de FOOTER view gestuurd, wil je een andere view? Ga naar /config/javascript.php en verander de default view!

        return view('pages.client', compact('client_info', 'client_list', 'today_tasks' , 'this_client'));
    }

    /*
	* create Client function
    */
    public function postStore(Request $request){
        $error = [];

        // If name is empty, return this error
        if(strlen($request->input('name')) <= 1){
            array_push($error, trans('messages.required_name_field') );
        }

        // If date of birth is empty, return this error
        if(strlen($request->input('dob')) <= 1){
            array_push($error, trans('messages.required_dob_field') );
        }

        // If color identifier is empty, return this error
        if(strlen($request->input('colour')) <= 1){
            array_push($error, trans('messages.required_color_field') );
        }

        if($error){
            return redirect()->back()->with('error', $error);
        }

    	try{
            $postData = $request->except('_token');
            $response = $this->guzzle_client->request('POST', $this->url . '/rest-api/add_patient/', [
                'json' => [
                    'caretaker_id' => 3,
                    'name' => $postData['name'],
                    'birth_date' => $postData['dob'],
                    'gender' => $postData['gender'],
                    'color_identifier' => $postData['colour'],
                    'language' => $postData['language']
                ]
            ]);
        }
        catch (RequestException $e){
            if ($e->getResponse()->getStatusCode() == '400') {
                // Redirect to root with error message
                return redirect()->back()->with('error', [ trans('messages.something_went_wrong') ]);
            }
        }

        return redirect()->back()->with('success', [ trans('messages.register_client_success') ]);
    }

    /*
    * Edit function to edit client profile information
    */
    public function getEdit($id){
        $client_info_response = $this->guzzle_client->get($this->url.'/rest-api/get_patient/'.$id, $this->user_credentials);

        // Get user client list
        $client_info = json_decode($client_info_response->getBody()->getContents());

        // return client edit view
        return view('pages.client.edit', compact('client_info'));
    }

    /*
    * Post the edit view to update the client profile information
    */
    public function postUpdate($id, Request $request) {
        $error = [];

        // error message for older users
        if($id < 13){
            array_push($error, trans('messages.update_failed') );
        }
        // If name is empty, return this error
        if(strlen($request->input('name')) <= 1){
            array_push($error, trans('messages.required_name_field') );
        }

        // If date of birth is empty, return this error
        if(strlen($request->input('dob')) <= 1){
            array_push($error, trans('messages.required_dob_field') );
        }

        // If color identifier is empty, return this error
        if(strlen($request->input('coloridentifier')) <= 1){
            array_push($error, trans('messages.required_color_field') );
        }

        if($error){
            return redirect()->back()->with('error', $error);
        }

    	try{
            $postData = $request->except('_token');
            $response = $this->guzzle_client->request('POST', $this->url . '/rest-api/edit_patient/', [
                // 'headers' => [
                //     'Authorization' => 'Token c311e6b04842576994ee0336268d6b32f4c88b26'
                // ],
                'form_params' => [
                    'caretaker_id' => 3,
                    'id' => $id,
                    'name' => $postData['name'],
                    'birth_date' => $postData['dob'],
                    'gender' => $postData['gender'],
                    'color_identifier' => $postData['coloridentifier'],
                    'language' => $postData['language'],
                    'panic_message' => '',
                    'panic_button_id' => ''
                ]
            ]);
        }
        catch (RequestException $e){
            if ($e->getResponse()->getStatusCode() == '400') {
                // Redirect to root with error message
                return redirect()->back()->with('error', [ trans('messages.something_went_wrong') ]);
            }
        }

        return redirect('/client/show/' . $id)->with('success', [ trans('messages.update_success') ]);
    }


    /*
    * Delete function for deleting client
    */
    public function postDestroy($id){
        return trans('messages.delete_client').$id.'';
    }

    public function postDate(Request $request){
        $client_info_response = $this->guzzle_client->get($this->url.'/rest-api/get_patient/'.$request->input('client_id'), $this->user_credentials);

        // Get user client list
        $client_info = json_decode($client_info_response->getBody()->getContents());

        // All tasks
        $task_list = $client_info->data->task_set;

        // Empty today tasks
        $today_tasks = [];

        // Loop through tasks
        foreach($task_list as $task){
            // Typecast object to array
            $task = (array)$task;

            // Check if in array, then push to today_tasks
            if($task['start_date'] == $request->input('date')) {
                $today_tasks[] = (object)$task;
            }
        }

        // Sort tasks by time
        usort($today_tasks, function($a, $b) {
            if ($b->start_time == "00:00:00") return false;
            return $a->start_time - $b->start_time;
        });

        $query = $request->input('date');

        return response()->json($today_tasks);
    }

}
