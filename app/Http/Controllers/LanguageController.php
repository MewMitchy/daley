<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App;

class LanguageController extends Controller
{
    public function index($language){
    	if($language == "nl" || $language == "en" || $language == "sr"){
       		session()->put('locale', $language);

       		return redirect('/client/show/13')->with('success', [ trans('messages.language_success') ]);
    	}
    	else{
    		
       		return redirect('/client/show/13')->with('error', [ trans('messages.language_failed') ]);
    	}
    }
}
