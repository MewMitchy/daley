<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;

use GuzzleHttp;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Response;

class AuthController extends Controller
{
    /*
    * Public variables
    * Use in controller: $this->variable
    */
    public $guzzle_client;
    public $url;


    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    public function __construct(Client $guzzle_client){
        // Check if the user is logged in, if yes, send back to daley page
        if(session('auth_token')){
            return redirect('/')->send();
        }
        // public variable values
        $this->guzzle_client = $guzzle_client;
        $this->url = 'http://bemika.webfactional.com';

        // save token for error "TokenMismatchException"
        $encrypter = app('Illuminate\Encryption\Encrypter');
        $encrypted_token = $encrypter->encrypt(csrf_token());

        if(session()->has('locale')){
            \App::setLocale(session()->get('locale'));
        }
    }

    public function getLogin(){
        // If an error has occured, return view with errors
        if(session('error')){
            return view('auth.login')->with('error', session('error'));
        }

        return view('auth.login');
    }

    public function postLogin(Request $request) {
        // Try to login
        try{
            // Get the input credentials
            $token_credentials = [
                'form_params' => [
                    'username' => $request->input('username'),
                    'password' => $request->input('password')
                ]
            ];

            // Make the call to get the user token
            $response = $this->guzzle_client->post($this->url.'/api-token-auth/', $token_credentials);

            // Get the user token
            $token = json_decode($response->getBody()->getContents());

            // Store the token in the session
            session(['auth_token' => $token->token]);

            // Return to the dashboard
            return redirect('/');
        }
        // If invalid credentials return this
        catch (RequestException $e){
            // If status code 400
            if ($e->getResponse()->getStatusCode() == '400') {
                // Redirect to root with error message
                return redirect()->back()->with('error', [ trans('messages.credentials') ]);
            }
        }

    }

    public function getRegister(){
        return view('auth.register');
    }

    /*
    * create new user
    * Method @request
    */
    public function postRegister(Request $request) {
        $error = [];
        
        // If name is empty, return this error
        if(strlen($request->input('name')) <= 1){
            array_push($error, trans('messages.required_name') );
        }

        // If email is empty, return this error
        if(strlen($request->input('email')) <= 1){
            array_push($error, trans('messages.required_email') );
        }

        // If username is shorter than 6 characters, return this error
        if(strlen($request->input('username')) <= 5){
            array_push($error, trans('messages.char_username') );
        }
        
        // If password is shorter than 6 characters, return this error
        if(strlen($request->input('password')) <= 5){
            array_push($error, 'Your password needs to be longer than 5 characters!');
        }

        if($error){
            return redirect()->back()->with('error', $error);
        }

        try{
            $post = $this->generateJsonPost($request->except('_token'));

            $response = $this->guzzle_client->request('POST', $this->url . '/rest-api/create_user/', [
                'json' => $post
            ]);
        }
        catch (RequestException $e){
            if ($e->getResponse()->getStatusCode() == '400') {
                // Redirect to root with error message
                return redirect()->back()->with('error', [ trans('messages.register_account_already') ]);
            }
        }

        // var_dump($response->getBody()->getContents());
        return redirect('/auth/login')->with('success', [ trans('messages.register_account_success') ]);

    }

    /**
     * Generate json data that's suitable for posting
     * @param array $postData
     * @return array
     */
    private function generateJsonPost($postData){
        $post = [
            'user' => [
                'username' => $postData['username'],
                'password' => $postData['password']
            ],
            'language' => 'NL',
            'name' => $postData['name'],
            'email' => $postData['email']
        ];

        return $post;
    }

    /*
    * Clear the session if user wanna logout @session()->flush()
    * Redirect to login page because there is no session set for logged in user
    */
    public function getLogout(Request $request){
        $request->session()->flush();
        return redirect('auth/login');
    }

    public function getForgot(){
        return view('auth.forgot');
    }

}
