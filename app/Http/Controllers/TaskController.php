<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use GuzzleHttp;
use Response;

class TaskController extends Controller
{
    /*
    * @method POST
    * post the task in the planning of a client
    */
    public function postTask(Request $request){
    	return redirect()->back()->with('success', [ trans('messages.task_success') ]);
    }

    /*
    * @method POST
    * Add a subtask to a task
    */
    public function postSubtask(Request $request){
    	return $request;
    }

    /*
    * @method POST
    * create your own custom task
    */
    public function postCustomTask(Request $request){
    	return $request;
    }

	public function postSearch(Request $request){
		$query = $request->except('_token');

        if (strlen($query['search']) == 0) return response()->json([]);

		if (strlen($query['search']) < 1) return response()->json([ trans('messages.no_results') ]);

		$tasks = [
			trans('task.shower'),
			trans('task.brush_teeth'),
            trans('task.have_breakfast'),
            trans('task.have_lunch'),
            trans('task.have_dinner'),
			trans('task.go_to_sleep'),
		];

		$response = preg_grep('/' . $query['search'] . '/', $tasks);

		return response()->json($response);
	}

}
