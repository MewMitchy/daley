<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;


class UserController extends Controller
{
    /*
    * Show user profile
    * Api doesn't show other users because of token
    */
    public function getShow(){
        // Call to user info
        $user_info_response = $this->guzzle_client->get($this->url.'/rest-api/get_user/', $this->user_credentials);

        // Get user information
        $user = json_decode($user_info_response->getBody()->getContents());

        return view('pages.user.profile', compact('user'));
    }

    /*
    * Update the user edit view profile information
    */
    public function postUpdate(Request $request){
        // Call to user info
        $user_info_response = $this->guzzle_client->get($this->url.'/rest-api/get_user/', $this->user_credentials);
        // Get user information
        $user = json_decode($user_info_response->getBody()->getContents());

        try{
            $postData = $request->except('_token');
            $response = $this->guzzle_client->request('POST', $this->url . '/rest-api/edit_caretaker/', [
                'form_params' => [
                    'id' => $user->data->id,
                    'user' => $user->data->id,
                    'name' => $postData['name'],
                    'email' => $postData['email'],
                    'language' => 1,
                    'time_zone' => $postData['timezone'],
                    'time_format' => $postData['timeformat'],
                    'active' => true
                ]
            ]);
            session()->put('locale', $postData['language']);
        }
        catch (RequestException $e){
            if ($e->getResponse()->getStatusCode() == '400') {
                // Redirect to root with error message
                return redirect()->back()->with('error', [ trans('messages.something_went_wrong') ]);
            }
        }

        return redirect()->back()->with('success', [ trans('messages.settings_success') ]);
    }


}