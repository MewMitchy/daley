<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class InsertController extends Controller
{
    public function addTask()
    {
        return "disabled";
        $task_name = 'Go to bed';
        $date = '2016-06-10';
        $time = '00:00:00';
        $task_id = 0;
        $subtask_list = [
            "Take off your clothes",
            "Put on your pajamas",
            "Turn off the lights",
            "Go to bed"
        ];
        $api_responses = [];

        $response = $this->guzzle_client->request('POST', $this->url . '/rest-api/add_task/', [
            'json' => [
                "patient" => 14,
                "caretaker" => 3,
                "title" => $task_name,
                "start_date" => $date,
                "start_time" => $time,
                "end_date" => $date
            ]
        ]);
        $body = $response->getBody()->getContents();
        $api_responses[] = $body;
        $task_id = json_decode($body)->data->id;

        foreach ($subtask_list as $subtask) {
            $response = $this->guzzle_client->request('POST', $this->url . '/rest-api/add_subtask/', [
                'json' => [
                    "task" => $task_id,
                    "title" => $subtask,
                    "duration" => 300,
                    "has_sound" => 0,
                    "sound_file_path" => "",
                    "pictogram_file_path" => "",
                    "order" => 0,
                    "active" => 1
                ]
            ]);
            $api_responses[] = $response->getBody()->getContents();
        }
        dd($api_responses);
    }
}
