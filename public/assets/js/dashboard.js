var timer;
// Replace calendar icons


jQuery(document).ready(function($) {
	deleteTask();
	taskSound();
	viewSubtasks();
	deleteSubtask();
	errorFadeIn();
	successFadeIn();
	lorreinFunctions();

	$(function  () {
		// $("ul#task-list").sortable();
	    $('.info-section.color').colorpicker({
	    	colorSelectors: {
                '#777777': '#777777',
                '#337ab7': '#337ab7',
                '#5cb85c': '#5cb85c',
                '#5bc0de': '#5bc0de',
                '#f0ad4e': '#f0ad4e',
                '#d9534f': '#d9534f'
            }
	    });
	    $('.form-group.colour').colorpicker({
	    	colorSelectors: {
                '#777777': '#777777',
                '#337ab7': '#337ab7',
                '#5cb85c': '#5cb85c',
                '#5bc0de': '#5bc0de',
                '#f0ad4e': '#f0ad4e',
                '#d9534f': '#d9534f'
            },
            color: '#777777',
            align: 'left'
	    });
	    $('.birthdate').datepicker({
		    toggleActive: true,
		    format: "yyyy-mm-dd"
		});
	});

	$('.glyphicon.glyphicon-chevron-left').replaceWith('<i class="ion-ios-arrow-back"></i>');
	$('.glyphicon.glyphicon-chevron-right').replaceWith('<i class="ion-ios-arrow-forward"></i>');

	// datepicker init
	$('#datetimepicker12').datetimepicker({
		inline: true,
		format:  "MM/dd/YYYY"
	});

	// format task time
	$('.task .time').each(function(index, el) {
		var time = $(this).html().substring(0, 5);
		$(this).html(time);
	});

	$('#search').submit(function(event) {
		event.preventDefault();
	})

	$('#search input[name="search"]').on('keyup', function(event) {
		event.preventDefault();

		timer = setTimeout(function() {
			var formData = {
				'search': $('#search input[name="search"]').val(),
				'_token': $('#search input[name="_token"]').val()
			}

			$.post( "/task/search", formData)
			.done(function( data ) {
				var results = '';
				$.each(data, function() {
					if(this == "No results"){
						results += '<li class="no-results" style="list-style:none;">' + this + '</li>';
					}
					else{
						results += '<li class="search-result">' + this + '<i class="ion-plus-round"></i></li>';
					}
				});
				$('#search-results').html(results);
				cloneTask(data);
			});
		}, 300);
	});

	$('#search input[name="search"]').on('keydown', function() {
		clearTimeout(timer);
	});

	function viewSubtasks(){

		$('.task-info, .task-icons').click(function(){
			$(this).parent('.task').find('.subtasks').toggleClass('hidden');
			$(this).parent('.task').toggleClass('subtask-shown');

			if ($(this).parent('.task').hasClass('subtask-shown')) {
				$(this).parent('.task').find('.task-icons i').removeClass('ion-arrow-down-b').addClass('ion-arrow-up-b');
			} else {
				$(this).parent('.task').find('.task-icons i').removeClass('ion-arrow-up-b').addClass('ion-arrow-down-b');
			};
		});
	}

	function taskSound() {
		$('.volume-icon').click(function(){
			if($(this).hasClass('ion-volume-medium')){
				$(this).removeClass('ion-volume-medium');
				$(this).addClass('ion-volume-mute');
			}
			else{
				$(this).removeClass('ion-volume-mute');
				$(this).addClass('ion-volume-medium');
			}
		});
	}

	function cloneTask() {
		$('#search-results li.search-result').on('click', function() {
			if ($(this).html() == 'no results') return;

			var task = $('.task:last').clone();

			var oldTime = $('.task .time').last().html();
			var newTime = moment(oldTime, "HH:mm").add(30, 'minutes').format('HH:mm');

			task.find('.name').html($(this).html());
			task.find('.name i').remove();
			task.find('.time').html(newTime);
			task.removeClass('hidden');

			$('.no-tasks').html('');

			$('.task-section #task-list').append(task);
			deleteTask();
			taskSound();
			viewSubtasks();
		});
	}
	function lorreinFunctions(){
    	$(".step_preferences").click(function(){
        	$("#overlay, #popup_step").fadeIn("1500");
    	});
     	$(".option .ion-clock").click(function(){
        	$("#overlay, #popup_task").fadeIn("1500");
    	});
		$(".delete").click(function(){
        	$("#overlay, #popup_delete").fadeIn("1500");
    	});
	    $(".secundairy_supervisor").click(function(){
	        $("#overlay, #popup_secundairy").fadeIn("1500");
	    });
	    $(".sluit").click(function(){
	        $("#overlay, #popup_step,  #popup_task, #popup_delete, #popup_secundairy").fadeOut("15001500");
	    });
	    $(".delete_left").click(function(){
	        $("#overlay, #popup_step,  #popup_task, #popup_delete, #popup_secundairy").fadeOut("15001500");
	    });
	    $(".delete_right").click(function(){
	        $("#overlay, #popup_step,  #popup_task, #popup_delete, #popup_secundairy").fadeOut("15001500");
	    });
    }

	function deleteTask(){
		$('.task-section #task-list .task .task-icons .ion-close-round').on('click', function() {
			$(this).parents('.task').remove();
			if($('#task-list li.task').length == '1'){
				$('p.no-tasks').html('This client does not have any tasks planned for today!');
			}
		});
	}

	$('#add-subtask').click(function(){
		var subtask = $(this).parents('.form-group').find('div.subtask-create:first').clone();
		var container = $(this).parents('.form-group');

		subtask.val('');

		container.append(subtask);
		deleteSubtask();
	});

	function deleteSubtask(){
		$('.delete-subtask').click(function(){
			if($('.subtask-create').length != 1){
				$(this).parents('.subtask-create').remove();
			}
		});
	}

	$('#datetimepicker12').on('dp.change', function(event) {
		var date = event.date.format('YYYY-MM-DD');

		getTasks(date);
	});
	$('#datetimepicker12').on('dp.update', function(event) {
		taskToDay();
	});

	function getTasks(date) {
		formData = {
			'date': date,
			'client_id': client_id,
			'_token': _token
		};

		$.post( "/client/date", formData)
		.done(function( data ) {
			if(data.length >= 1){
				$('.no-tasks').html('');
			}
			else{
				$('.no-tasks').html('This client does not have any tasks planned for today!');
			}
			$("#task-list .task:not(.hidden)").remove();
			$.each(data, function() {
				var task = $('.task:last').clone();
				var subtask = '';
				var preferences = '';
				var newTime = moment(this.start_time, "HH:mm").format('HH:mm');
				task.find('.subtasks ul').html('');
				task.removeClass('hidden');
				task.find('.name').html(this.title);
				task.find('.time').html(newTime);
				$.each(this.subtask_set, function() {
					subtask += '<div class="icon_img"><img src="http://placehold.it/50x50" class="icon"></div>';
					subtask += '<li class="subtask col-md-10">' + this.title + '</li>';
					subtask += '<li class="step_preferences col-md-2"><i class="ion-android-settings"></i></li>';
					subtask += '<div style="clear: both"></div>';
				});
				task.find('.subtasks ul').append(subtask);
				$('#task-list').append(task);
			});
			deleteTask();
			taskSound();
			viewSubtasks();
			lorreinFunctions();
		});
	}
	// POP FADEIN/OUT
	$('.errors .ion-close').on('click', function(event) {
		event.preventDefault();
		errorFadeOut();
	});
	$('.success .ion-close').on('click', function(event) {
		event.preventDefault();
		successFadeOut();
	});
	function errorFadeIn() {
		$('.errors').fadeIn();
	}
	function successFadeIn() {
		$('.success').fadeIn();
	}
	function errorFadeOut() {
		$('.errors').fadeOut();
	}
	function successFadeOut() {
		$('.success').fadeOut();
	}

	function taskToDay(){
		$(".date_count_style").remove();
		$("head").append($('<style class="date_count_style"></style>'));
		jQuery.each( taken, function( i, val ) {
			var eachday = moment(val.start_date).format('MM/DD/YYYY');
			$(".date_count_style").append('[data-day="'+eachday+'"]:after { content: \'' + task_count[val.start_date] + '\' }');
		});
	}
	taskToDay();
});
