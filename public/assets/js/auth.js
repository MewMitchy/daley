jQuery(document).ready(function($) {
	errorFadeIn();
	successFadeIn();
		
	var showPass = 	$('.showpass'),
		passInput = $('input.password')

	showPass.mousedown(function(event) {
		event.preventDefault();
		/* Act on the event */
		if (passInput.attr('type', 'password')) {
			passInput.attr('type', 'text');
		} 
		else {
			passInput.attr('type', 'text');
		};

	});
	showPass.mouseup(function(event) {
		event.preventDefault();
		/* Act on the event */

		if (passInput.attr('type', 'password')) {
			return;
		} 
		else {
			passInput.attr('type', 'password');
		};

	});
	// POP FADEIN/OUT
	$('.errors .ion-close').on('click', function(event) {
		event.preventDefault();
		errorFadeOut();
	});
	$('.success .ion-close').on('click', function(event) {
		event.preventDefault();
		successFadeOut();
	});
	function errorFadeIn() {
		$('.errors').fadeIn();
	}
	function successFadeIn() {
		$('.success').fadeIn();
	}
	function errorFadeOut() {
		$('.errors').fadeOut();
	}
	function successFadeOut() {
		$('.success').fadeOut();
	}

});