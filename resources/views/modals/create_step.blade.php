<div class="modal create_client fade" id="create_step" tabindex="-1" role="dialog" aria-labelledby="create_step_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="create_step_label">{{ trans('step.create') }}</h4>
      </div>
      <div class="modal-body">
        {{ Form::open(array('action' => 'TaskController@postTask' )) }}
			<div class="form-group">
				<label for="step">{{ trans('step.name') }}</label>
				<input type="text" name="step" placeholder="{{ trans('step.name') }}" class="form-control non-full">
			</div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{ trans('global.save_button') }}</button>
        <button type="button" class="btn btn-secondary cancel" data-dismiss="modal">{{ trans('global.cancel_button') }}</button>
      </div>
		{{ Form::close() }}
    </div>
  </div>
</div>