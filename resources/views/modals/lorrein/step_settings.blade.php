<div id="popup_step">
	<div class="step_header">
		<h3 class="title_step_popup">{{ trans('messages.step_preferences') }}</h3>
		<div class="sluit"><h3 class="sluit">x</h3></div>
	</div>
	<div class="step_bg container">
		<div class="step_options row">
			<div class="{{-- step_icon --}} col-md-6">
				<h4 class="step_content_popup">{{ trans('messages.step_description') }}</h4>
				<textarea class="description" placeholder="{{ trans('messages.your_step_description') }}"></textarea>
				<h4 class="step_content_popup">{{ trans('messages.icon_upload') }}</h4>
				<p class="timer">{{ trans('messages.specifications') }}: 45 x 45 pixels 0,5 MB</p>
				<img src="http://placehold.it/60x60"> &nbsp;&nbsp; <div class="button-submit upload">{{ trans('messages.icon_upload') }}</div>
			</div>
			<div class="{{-- step_time --}} col-md-6">
        <div class="col-md-12">
  				<h4 class="step_content_popup">{{ trans('messages.time') }}</h4>
  				<p class="timer">07:00 - 07:05</p>
  				<form action="action_page.php">
  					<select name="timer" class="timer_dropdown">
  						<option value="5minutes">5 {{ trans('messages.minutes') }}</option>
  						<option value="10minutes">10 {{ trans('messages.minutes') }}</option>
  						<option value="15minutes">15 {{ trans('messages.minutes') }}</option>
  						<option value="20minutes">20 {{ trans('messages.minutes') }}</option>
  						<option value="25minutes">25 {{ trans('messages.minutes') }}</option>
  						<option value="30minutes">30 {{ trans('messages.minutes') }}</option>
  						<option value="35minutes">35 {{ trans('messages.minutes') }}</option>
  						<option value="40minutes">40 {{ trans('messages.minutes') }}</option>
  						<option value="45minutes">45 {{ trans('messages.minutes') }}</option>
  						<option value="50minutes">50 {{ trans('messages.minutes') }}</option>
  						<option value="55minutes">55 {{ trans('messages.minutes') }}</option>
  						<option value="60minutes">60 {{ trans('messages.minutes') }}</option>
  					</select>
  				</form>
        </div>
				<div class="col-md-6">
					<h4 class="step_content_popup">{{ trans('messages.timer') }}</h4>
					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
						<label class="onoffswitch-label" for="myonoffswitch"></label>
					</div>
				</div>
				<div class="col-md-6">
					<h4 class="step_content_popup">{{ trans('messages.repeat') }}</h4>
					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
						<label class="onoffswitch-label" for="myonoffswitch"></label>
					</div>
				</div>
				<div class="col-md-6 duplicate-step">
					<h4 class="step_content_popup">{{ trans('messages.duplicate') }}</h4>
					<a href="#"><i class="ion-ios-copy"></i></a>
				</div>
				<div class="col-md-6">
					<h4 class="step_content_popup">{{ trans('messages.sound') }}</h4>
					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox myonoffswitch" id="myonoffswitch" checked>
						<label class="onoffswitch-label" for="myonoffswitch"></label>
					</div>
				</div>
			</div> {{-- end col-6 --}}
		</div> {{-- end row --}}
	</div> {{-- end container --}}
</div> {{-- end popup step --}}
