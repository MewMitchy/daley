<div id="popup_task">

	<div class="task_popup">
            <div class="task_header">
              <h3 class="title_step_popup">{{ trans('messages.task_preferences') }}</h3>
              <div class="sluit"><h3 class="sluit">x</h3></div>
            </div>
            <div class="task_bg">
              <div class="task_options">
                <div class="task_description">
                  <h4 class="task_content_popup">{{ trans('messages.task_description') }}</h4>
                  <textarea class="description" placeholder="{{ trans('messages.your_task_description') }}"></textarea>
                </div>
                <div class="step_time">
                  <h4 class="task_content_popup">{{ trans('messages.time') }}</h4>
                  <p class="timer">07:00 - 07:05</p>
                    <form action="action_page.php">
                      <select name="timer" class="timer_dropdown">
                        <option value="5minutes">5 {{ trans('messages.minutes') }}</option>
                        <option value="10minutes">10 {{ trans('messages.minutes') }}</option>
                        <option value="15minutes">15 {{ trans('messages.minutes') }}</option>
                        <option value="20minutes">20 {{ trans('messages.minutes') }}</option>
                        <option value="25minutes">25 {{ trans('messages.minutes') }}</option>
                        <option value="30minutes">30 {{ trans('messages.minutes') }}</option>
                        <option value="35minutes">35 {{ trans('messages.minutes') }}</option>
                        <option value="40minutes">40 {{ trans('messages.minutes') }}</option>
                        <option value="45minutes">45 {{ trans('messages.minutes') }}</option>
                        <option value="50minutes">50 {{ trans('messages.minutes') }}</option>
                        <option value="55minutes">55 {{ trans('messages.minutes') }}</option>
                        <option value="60minutes">60 {{ trans('messages.minutes') }}</option>
                      </select>
                    </form>
                  </div>
                  <div class="task_repeat">
                  <h4 class="task_content_popup">{{ trans('messages.repeat_task') }}</h4>
                    <form action="action_page.php">
                      <select name="timer" class="timer_dropdown">
                        <option value="5minutes">{{ trans('messages.dont_repeat') }}</option>
                        <option value="5minutes">{{ trans('messages.daily') }}</option>
                        <option value="10minutes">{{ trans('messages.weekly') }}</option>
                        <option value="15minutes">{{ trans('messages.monthly') }}</option>
                        <option value="20minutes">{{ trans('messages.annually') }}</option>
                      </select>
                    </form>
                  </div>
                   <div class="task_color">
                      <h4 class="step_content_popup">{{ trans('messages.client_color') }}</h4>
                        <div class="color_row">
                          <button style="background:#ab47bc; border: 0; border-radius: 50px; height: 20px; width: 20px;" onClick="document.getElementById('foo')
                          .jscolor.fromString('ab47bc')"></button>

                          <button style="background:#ed145b; border: 0; border-radius: 50px; height: 20px; width: 20px; margin-left: 10px;"  onClick="document.getElementById('foo')
                          .jscolor.fromString('ed145b')"></button>

                          <button style="background:#ec008c; border: 0; border-radius: 50px; height: 20px; width: 20px; margin-left: 10px;"  onClick="document.getElementById('foo')
                          .jscolor.fromString('ec008c')"></button>

                          <button style="background:#f26d7d; border: 0; border-radius: 50px; height: 20px; width: 20px; margin-left: 10px;" onClick="document.getElementById('foo')
                          .jscolor.fromString('f26d7d')"></button>

                          <button style="background:#ff9800; border: 0; border-radius: 50px; height: 20px; width: 20px; margin-left: 10px;"  onClick="document.getElementById('foo')
                          .jscolor.fromString('ff9800')"></button>

                          <button style="background:#2e3192; border: 0; border-radius: 50px; height: 20px; width: 20px; margin-left: 10px;"  onClick="document.getElementById('foo')
                          .jscolor.fromString('2e3192')"></button>

                          <button style="background:#0054a6; border: 0; border-radius: 50px; height: 20px; width: 20px; margin-left: 10px;"  onClick="document.getElementById('foo')
                          .jscolor.fromString('0054a6')"></button>
                        </div>
                      <br/>
                      <div class="color_row">
                        <button style="background:#00aeef; border: 0; border-radius: 50px; height: 20px; width: 20px;" onClick="document.getElementById('foo')
                        .jscolor.fromString('00aeef')"></button>

                        <button style="background:#00a651; border: 0; border-radius: 50px; height: 20px; width: 20px; margin-left: 10px;"  onClick="document.getElementById('foo')
                        .jscolor.fromString('00a651')"></button>

                        <button style="background:#f26522; border: 0; border-radius: 50px; height: 20px; width: 20px; margin-left: 10px;"  onClick="document.getElementById('foo')
                        .jscolor.fromString('f26522')"></button>

                        <button style="background:#ed1c24; border: 0; border-radius: 50px; height: 20px; width: 20px; margin-left: 10px;" onClick="document.getElementById('foo')
                        .jscolor.fromString('ed1c24')"></button>

                        <button style="background:#fff200; border: 0; border-radius: 50px; height: 20px; width: 20px; margin-left: 10px;"  onClick="document.getElementById('foo')
                        .jscolor.fromString('fff200')"></button>

                        <button style="background:#00a99d; border: 0; border-radius: 50px; height: 20px; width: 20px; margin-left: 10px;"  onClick="document.getElementById('foo')
                        .jscolor.fromString('00a99d')"></button>

                        <button style="background:#3cb878; border: 0; border-radius: 50px; height: 20px; width: 20px; margin-left: 10px;"  onClick="document.getElementById('foo')
                        .jscolor.fromString('3cb878')"></button>
                      </div>

                      <!-- <p><input class="jscolor" id="foo"></p> -->

                  </div>
                  <div class="task_copy">
                     <h4 class="step_content_popup">{{ trans('messages.print_task') }}</h4>
                     &nbsp;
                     <a href="#"><i class="ion-printer"></i></a>
                  </div>
                   <div class="task_sound">
                     <h4 class="task_content_popup">{{ trans('messages.sound') }}</h4>
                     <div class="onoffswitch">
                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox myonoffswitch" id="myonoffswitch" checked>
                      <label class="onoffswitch-label" for="myonoffswitch"></label>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</div> <!-- END TASK PREFERENCES POP UP -->