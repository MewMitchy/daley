<div id="popup_delete">
	<div class="delete_popup"> <!-- DELETE POP UP WARNING -->
	    <div class="delete_header">
	      <h3 class="title_popup">{{ trans('messages.sure_delete') }}</h3>
	      <div class="sluit"><h3 class="sluit">x</h3></div>
	    </div>
	    <div class="delete_bg">
	    <div class="delete_options"><h4 class="delete_left">{{ trans('messages.yes_delete') }}</h4><h4 class="delete_right">{{ trans('messages.no_delete') }}</h4></div>
	    </div>
	</div>
</div> <!-- END DELETE POP UP WARNING-->