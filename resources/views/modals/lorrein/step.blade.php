<form method="GET" class="form_schedule form-horizontal hidden">
						<input id="id_task_id" name="task_id" type="hidden" value="266">
						<input id="id_days" name="days" type="hidden">

						<div class="form-group">
							<label for="id_start_date" class="col-sm-4 control-label">Start Date:</label>
							<div class="col-sm-8" id="div_id_start_date">
								<!--<div id="dtp_start_date"></div>-->
								<input class="form-control" id="id_start_date" name="start_date" type="text">
								<span class="error-msg" id="spn_id_start_date"></span>
							</div>
						</div>
						<div class="form-group">
							<label for="id_start_time" class="col-sm-4 control-label">Start Time:</label>
							<div class="col-sm-8" id="div_id_start_time">
								<input class="form-control" id="id_start_time" name="start_time" type="text">
								<span class="error-msg" id="spn_id_start_time"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Days:</label>
							<div class="col-sm-8" id="div_id_days">
								<div class="checkbox">
									<label><input type="checkbox" id="chk_0" class="day1">M</label>
									<label><input type="checkbox" id="chk_1" class="day1">T</label>
									<label><input type="checkbox" id="chk_2" class="day1">W</label>
									<label><input type="checkbox" id="chk_3" class="day1">T</label>
									<label><input type="checkbox" id="chk_4" class="day1">F</label>
									<label><input type="checkbox" id="chk_5" class="day1">S</label>
									<label><input type="checkbox" id="chk_6" class="day1">S</label>
								</div>
								<span class="error-msg" id="spn_id_days"></span>
							</div>
						</div>

						<div class="form-group">
							<label for="id_repeat" class="col-sm-4 control-label">Repeat:</label>
							<div class="col-sm-8" id="div_id_repeat">
								<select class="form-control" id="id_repeat" name="repeat">
									<option value="0">Weekly</option>
									<option value="1">Monthly</option>
									<option value="2">Yearly</option>
								</select>
								<span class="error-msg" id="spn_id_repeat"></span>
							</div>
						</div>

						<div class="form-group">
							<label for="id_end_date" class="col-sm-4 control-label">End Date:</label>
							<div class="col-sm-8" id="div_id_end_date">
								<input class="form-control" id="id_end_date" name="end_date" type="text">
								<span class="error-msg" id="spn_id_end_date"></span>
							</div>
						</div>
						<button class="btn btn-primary" id="btn_save_schedule" type="button">Save</button>
					</form>