<div class="modal create_client fade" id="add_supervisor" tabindex="-1" role="dialog" aria-labelledby="create_task_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content create_client">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="create_task_label">{{ trans('task.secondary') }}</h4>
      </div>
      <div class="modal-body">
        {{ Form::open(array('action' => 'TaskController@postTask' )) }}
			<div class="form-group name">
				<label for="task">{{ trans('task.name') }}</label>
				<input type="text" name="task" placeholder="{{ trans('task.name') }}" class="form-control non-full">
			</div>
      <div class="form-group email">
        <label for="task">{{ trans('task.email') }}</label>
        <input type="text" name="task" placeholder="{{ trans('task.email') }}" class="form-control non-full">
      </div>
      <div class="form-group tel">
        <label for="task">{{ trans('task.tel') }}</label>
        <input type="number" name="task" placeholder="{{ trans('task.tel') }}" class="form-control non-full">
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{ trans('global.save_button') }}</button>
        <button type="button" class="btn btn-secondary cancel" data-dismiss="modal">{{ trans('global.cancel_button') }}</button>
      </div>
		{{ Form::close() }}
    </div>
  </div>
</div>
</div>
