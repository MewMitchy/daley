<div class="modal create_client fade" id="create_client" tabindex="-1" role="dialog" aria-labelledby="create_client_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content create_client">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="create_client_label">{{ trans('client.new_client') }}</h4>
      </div>
      <div class="modal-body">
        {{ Form::open(array('action' => 'ClientController@postStore' )) }}
			<div class="form-group name">
				<label for="name">{{ trans('client.name') }}</label>
				<input type="text" name="name" class="form-control">
			</div>
			<div class="form-group dob">
				<label for="dob">{{ trans('client.date_of_birth') }}</label>
				<input type="text" name="dob" class="form-control birthdate">
			</div>
			<div class="form-group gender">
				<label for="gender">{{ trans('client.gender') }}</label><br>
				<label class="radio-inline"><input type="radio" name="gender" value="M">{{ trans('client.male') }}</label>
				<label class="radio-inline"><input type="radio" name="gender" value="F">{{ trans('client.female') }}</label>
			</div>
			<div class="form-group colour input-group colorpicker-component">
				<label for="colour">{{ trans('client.color_identifier') }}</label>
				<input type="text" name="colour" class="form-control colorpicker">
				<span class="input-group-addon"><i></i></span>
			</div>
			<div class="form-group language">
				<label for="language">{{ trans('client.language') }}</label><br>{{--
				<label class="radio-inline"><input type="radio" name="language" value="1" checked>English</label>
				<label class="radio-inline"><input type="radio" name="language" value="2">Nederlands</label> --}}
				<select name="language">
					<option value="1">{{ trans('client.english') }}</option>
					<option value="2">{{ trans('client.dutch') }}</option>
					<option value="3">{{ trans('client.serbian') }}</option>
				</select>
			</div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary"><i class="ion-plus"></i>Add client</button>
        <button type="button" class="btn btn-secondary cancel" data-dismiss="modal">{{ trans('global.cancel_button') }}</button>
      </div>
		{{ Form::close() }}
    </div>
  </div>
</div>
