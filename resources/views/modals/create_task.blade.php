<div class="modal create_client fade" id="create_task" tabindex="-1" role="dialog" aria-labelledby="create_task_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="create_task_label">{{ trans('task.create') }}</h4>
      </div>
      <div class="modal-body">
        {{ Form::open(array('action' => 'TaskController@postTask' )) }}
			<div class="form-group">
				<label for="task">{{ trans('task.name') }}</label>
				<input type="text" name="task" placeholder="{{ trans('task.name') }}" class="form-control non-full">
			</div>
			<div class="form-group">
				<span>
					<label for="subtask">{{ trans('task.subtask') }}</label>
					<i class="ion-plus-round pull-right" id="add-subtask"></i>
				</span>
        <div class="subtask-create">
				  <input type="text" name="subtask" placeholder="{{ trans('task.title') }}" class="form-control create-subtask">
          <i class="ion-trash-b delete-subtask"></i>
        </div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{ trans('global.save_button') }}</button>
        <button type="button" class="btn btn-secondary cancel" data-dismiss="modal">{{ trans('global.cancel_button') }}</button>
      </div>
		{{ Form::close() }}
    </div>
  </div>
</div>