<div class="modal create_client fade" id="user_settings" tabindex="-1" role="dialog" aria-labelledby="user_settings_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="create_task_label">{{ trans('user.settings') }}</h4>
      </div>
      <div class="modal-body">
        {{ Form::open(array('action' => 'UserController@postUpdate' )) }}
      <div class="form-group">
          <label for="name"  class="primairy_content_popup">{{ trans('user.name') }}</label>
          <input type="text" name="name" placeholder="{{ trans('user.name') }}" class="form-control" value="{{$user_info->data->name}}">
      </div>
      <div class="form-group">
          <label for="email" class="primairy_content_popup">{{ trans('user.mail') }}</label>
          <input type="text" name="email" placeholder="{{ trans('user.mail') }}" class="form-control" value="{{$user_info->data->email}}">
      </div>
      <div class="form-group">
          <label for="tel" class="primairy_content_popup">{{ trans('user.tel') }}</label>
          <input type="number" name="tel" placeholder="{{ trans('user.tel') }}" class="form-control" >
      </div>
      <div class="form-group">
          <label for="timezone" class="primairy_content_popup">{{ trans('user.timezone') }}</label>
          <select name="timezone" class="form-control">
            <option value="Europe/Vienna">{{ trans('user.europe') }}/{{ trans('user.vienna') }}</option>
            <option value="America/Caracas">{{ trans('user.america') }}/{{ trans('user.caracas') }}</option>
          </select>
      </div>
      <div class="form-group">
          <label for="timeformat" class="primairy_content_popup">{{ trans('user.timeformat') }}</label><br>
          <label class="radio-inline"><input type="radio" name="timeformat" value="24"
          @if($user_info->data->time_format == "24")
            checked
          @endif
          >24:00</label>
          <label class="radio-inline"><input type="radio" name="timeformat" value="12"
          @if($user_info->data->time_format == "12")
            checked
          @endif
          >12:00</label>
      </div>
      <div class="form-group">
          <label for="language">{{ trans('user.language') }}</label><br>
                      <label class="radio-inline"><input type="radio" name="language" value="en"
                      @if(session()->get('locale') == "en")
                        checked
                      @endif
                      > {{ trans('client.english') }}</label>
                      <label class="radio-inline"><input type="radio" name="language" value="nl"
                      @if(session()->get('locale') == "nl")
                        checked
                      @endif
                      > {{ trans('client.dutch') }}</label>
                      <label class="radio-inline"><input type="radio" name="language" value="sr"
                      @if(session()->get('locale') == "sr")
                        checked
                      @endif
                      > {{ trans('client.serbian') }}</label>
      </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{ trans('global.save_button') }}</button>
        <button type="button" class="btn btn-secondary cancel" data-dismiss="modal">{{ trans('global.cancel_button') }}</button>
      </div>
    {{ Form::close() }}
    </div>
  </div>
</div>