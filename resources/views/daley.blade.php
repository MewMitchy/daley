@include('layouts.dashboard.head')
<!-- Header -->
<!-- @include('layouts.dashboard.header') -->
	@yield('navigation')
<!-- Content -->
	@include('messages.errors')
	@include('messages.success')

	@include('layouts.dashboard.navigation')
	
	@yield('content')
<!-- Footer -->
@include('layouts.dashboard.footer')