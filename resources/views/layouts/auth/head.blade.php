<!DOCTYPE html>
<html>
<head>
	<title>Daley</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Stylesheets -->
	<!-- <link href="{{ asset('/assets/css/auth.css') }}" rel="stylesheet"> -->
	<link href="{{ asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/ionicons.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/fonts.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/mourad.css') }}" rel="stylesheet">
	<link rel="icon" type="image/ico" href="/assets/images/daley_fav.ico"/>
</head>
<body>