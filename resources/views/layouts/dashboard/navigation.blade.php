<header>
	<div class="container-fluid header client">
		<div class="row">
			<div class="col-md-8 col-xs-5">
				<a href="/"><img src="/assets/images/logo-white-2.svg" class="header-logo" alt="Logo Daley" height="65"></a>
			</div>
			<div class="col-md-4">
				<div class="dropdown-user">
					<button type="button" data-toggle="dropdown">{{ $user_info->data->name }}&nbsp;&nbsp; <span class="caret"></span></button>
					<ul class="dropdown-menu">
						<li><a href="#" data-toggle="modal" data-target="#user_settings" id="data-target">{{ trans('nav.settings') }}</a></li>
						<li><a href="/auth/logout">{{ trans('nav.logout') }}</a></li>
					 </ul>
				</div>
			</div>
		</div>
	</div>
</header>




<!-- User settings modal -->
@include('modals.user_settings')

<!-- Create Task Modal -->
@include('modals.create_task')
