<!DOCTYPE html>
<html>
<head>
	<title>Daley</title>
	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Stylesheets -->
	<link href="{{ asset('/assets/css/bootstrap-colorpicker.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/bootstrap-datepicker3.css') }}" rel="stylesheet">

	<link href="{{ asset('/assets/css/fonts.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/ionicons.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/mitch.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/lorrein.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/yasmine.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/mourad-dashboard.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/dashboard.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/bruno.css') }}">
	<link rel="icon" type="image/ico" href="/assets/images/daley_fav.ico">

	<script src="/assets/js/jquery.1.11.13.min.js" type="text/javascript" ></script>
	<script type="text/javascript" src="/bower_components/moment/min/moment.min.js"></script>
	<!-- <script type="text/javascript" src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script> -->
	<script type="text/javascript" src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
	<link rel="stylesheet" href="/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
</head>
<body>
