	<!-- Javascript -->
	<script type="text/javascript" src="{{ asset('/assets/js/sortable.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/assets/js/bootstrap-colorpicker.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/assets/js/bootstrap-datepicker.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/assets/js/script.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/assets/js/dashboard.js') }}"></script>
</body>
</html>
