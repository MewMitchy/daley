@include('layouts.auth.head')
<!-- Header -->
@include('layouts.auth.header')
<!-- Content -->
<div class="container">
	@include('messages.errors')
	@include('messages.success')
	<div id="login" class="col-md-6 col-xs-12">

		<div class="row">
			<div class="center-block">
				<img src="/assets/images/Daley_Logo.svg" width="80%">

	@yield('content')

			</div>
		</div>
	</div>

	<div class="col-md-6 hidden-xs hidden-sm">
		<div class="row">
			<img class="image" src="/assets/images/daley1.png">
		</div>
	</div>

	<footer>
		<p>Copyright &copy; 2016 Daley 	-  All rights reserved</p>
	</footer>

</div>
<!-- Footer -->
@include('layouts.auth.footer')