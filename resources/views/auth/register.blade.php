@extends('auth')

@section('content')

{{  Form::open(array('action' => 'Auth\AuthController@postRegister' )) }}

	<input type="text" name="name" placeholder="{{ trans('auth.name') }}">
	<input type="text" name="username" placeholder="{{ trans('auth.username') }}">
	<input type="text" name="email" placeholder="{{ trans('auth.email') }}">
	<input type="password" name="password" placeholder="{{ trans('auth.password') }}">
	<input type="submit" value="{{ trans('auth.signup') }}" />


	<p>{{ trans('auth.already_signed_up') }} <a href="/auth/login"> {{ trans('auth.login_here') }}</a></p>
{{ Form::close() }}

@endsection
