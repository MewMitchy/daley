@extends('auth')

@section('content')

{{  Form::open(array('action' => 'Auth\AuthController@postLogin' )) }}

	<input type="text" name="username" placeholder="{{ trans('auth.username') }}">
	<input type="password" name="password" placeholder="{{ trans('auth.password') }}">
	<input type="submit" value="{{ trans('auth.login') }}" />


	<p>{{ trans('auth.no_account') }} <a href="/auth/register"> {{ trans('auth.signup') }}</a></p>
	<p><a href="/auth/forgot">{{ trans('auth.forgot') }}</a></p>

{{ Form::close() }}

@endsection
