@extends('auth')

@section('content')

{{  Form::open(array('action' => 'Auth\AuthController@postLogin' )) }}
	<input name="email" placeholder="{{ trans('auth.email') }}" type="text" class="login-input">
	<input type="submit" value="{{ trans('auth.request_password') }}">

	<p>{{ trans('auth.no_account') }} <a href="/auth/register"> {{ trans('auth.signup') }}</a></p>
{{ Form::close() }}
@endsection
