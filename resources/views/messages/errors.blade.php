@if(session('error'))
	<div class="errors" style="display:none">
		@foreach(session('error') as $error)
				{{$error}}<br/>
		@endforeach
		<i class="ion-close"></i>
	</div>
@endif
