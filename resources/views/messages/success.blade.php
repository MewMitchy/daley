@if(session('success'))
	<div class="success">
		@foreach(session('success') as $success)
			{{$success}}
		@endforeach
		<i class="ion-close"></i>
	</div>
@endif
