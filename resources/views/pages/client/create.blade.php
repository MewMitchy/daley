@extends('daley')

@section('content')

<div class="col-sm-8 col-sm-offset-2"> 

        {{ Form::open(array('action' => 'ClientController@postStore' )) }}
			<div class="form-group name">
				<label for="name">{{ trans('client.name') }}</label>
				<input type="text" name="name" class="form-control">
			</div>
			<div class="form-group dob">
				<label for="dob">{{ trans('client.date_of_birth') }}</label>
				<input type="text" name="dob" class="form-control birthdate">
			</div>
			<div class="form-group gender">
				<label for="gender">{{ trans('client.gender') }}</label><br>
				<label class="radio-inline"><input type="radio" name="gender" value="M">Male</label>
				<label class="radio-inline"><input type="radio" name="gender" value="F">Female</label>
			</div>
			<div class="form-group colour">
				<label for="colour">{{ trans('client.color_identifier') }}</label>
				<input type="text" name="colour" class="form-control colorpicker">
			</div>
			<div class="form-group language">
				<label for="language">{{ trans('client.language') }}</label><br>{{-- 
				<label class="radio-inline"><input type="radio" name="language" value="1" checked>English</label>
				<label class="radio-inline"><input type="radio" name="language" value="2">Nederlands</label> --}}
				<select name="language">
					<option value="1">English</option>
					<option value="2">Dutch</option>
				</select>
			</div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary"><i class="ion-plus"></i>Add client</button>
        <button type="button" class="btn btn-secondary cancel" data-dismiss="modal">{{ trans('global.cancel_button') }}</button>
      </div>
		{{ Form::close() }}
    
</div>
@endsection