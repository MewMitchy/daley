<!-- Extend the Daley view -->
@extends('daley')
<!-- Content -->
@section('content')

{{ Form::open(array('action' => 'TaskController@postTask' )) }}
	<div class="col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1">
		<div class="form-group">
			<label for="task">Task</label>
			<input type="text" name="task" placeholder="Title" class="form-control">
		</div>
		<div class="form-group">
			<span>
				<label for="subtask">Subtasks</label>
				<i class="ion-plus-round pull-right" id="add-subtask"></i>
			</span>
			<input type="text" name="subtask" placeholder="Title" class="form-control create-subtask">
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-primary btn-lg" value="Save changes">
			<input type="submit" class="btn btn-danger" value="Discard changes">
		</div>
	</div>
{{ Form::close() }}


<!-- End section -->
@endsection