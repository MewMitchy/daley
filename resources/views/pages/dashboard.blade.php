@extends('daley')

@section('content')
	@include('modals.create_client')

	<div class="container">
		<div class="row">
			<div class="col-md-12 client-section">
				<ul class=" client-list">
					@foreach($this_client as $client)

						<a href="/client/show/{{ $client['id'] }}" class="client">
							<li>
								<div class="client-color-list" style="background-color:{{ $client['color']}}"></div>
								<div class="client-name">{{ $client['name'] }}</div>
							</li>
						</a>
					@endforeach
					<li class="add-client-wrapper">
						<a href="#" class="add-client popup-modal" data-toggle="modal" data-target="#create_client">
							<i class="ion-plus"></i>{{ trans('client.add_client') }}
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

@endsection
