@extends('daley')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 client-section">
			<div class="dropdown">

				<button class="client-list-button dropdown-toggle" type="button" data-toggle="dropdown">
					<div class="client-color" style="background-color:{{ $client_info->data->color_identifier }}"></div>
					<p>{{ $client_info->data->name }} </p>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu client-list">
					@foreach($this_client as $client)
					<a href="/client/show/{{ $client['id'] }}" class="client">
						<li>
							<div class="client-color-list" style="background-color:{{ $client['color']}}"></div>
							<div class="client-name">{{ $client['name'] }}</div>
						</li>
					</a>
					@endforeach
					<li class="add-client-wrapper">
						<a href="#" class="add-client popup-modal"= data-toggle="modal" data-target="#create_client">
							<i class="ion-plus"></i>{{ trans('client.add_client') }}
						</a>
					</li>
				</ul>
			</div>

			{{ Form::open(array('action' => array('ClientController@postUpdate', $client_info->data->id), 'class' => 'client-info' )) }}
			<div class="info-section name">
				<label for="name" class="info-title">{{ trans('client.name') }}</label>
				<input type="text" name="name" class="info" value="{{ $client_info->data->name }}"/>
			</div>
			<div class="info-section birthday">
				<label for="dob" class="info-title">{{ trans('client.date_of_birth') }}</label>
				<input type="text" name="dob" class="info birthdate" value="{{ $client_info->data->birth_date }}"></input>
			</div>
			<div class="info-section gender">
				<label for="gender" class="info-title">{{ trans('client.gender') }}</label>
				<select name="gender" class="info">
					<option value="M">M</option>
					<option value="F">F</option>
				</select>
			</div>
			<div class="info-section color input-group colorpicker-component">
				<label for="coloridentifier" class="info-title">{{ trans('client.color_identifier') }}</label>
				<input type="text" name="coloridentifier" class="info colorpicker" value="{{ $client_info->data->color_identifier }}"/>
				<span class="input-group-addon"><i></i></span>
			</div>
			<div class="info-section language">
				<label for="language" class="info-title">{{ trans('client.language') }}</label>
				<select name="language" class="info">
					<option value="1">{{ trans('client.english') }}</option>
					<option value="2">{{ trans('client.dutch') }}</option>
					<option value="3">{{ trans('client.serbian') }}</option>
				</select>
			</div>
			<div class="info-section panicbutton">
				<label for="panicbutton" class="info-title">{{ trans('client.panic_button') }}</label>
				<select name="panicbutton" class="info">
					<option value="1">{{ trans('client.panic_call') }}</option>
					<option value="2">{{ trans('client.panic_text') }}</option>
					<option value="3">{{ trans('client.panic_mail') }}</option>
				</select>
			</div>

			<div class="info-section supervisor">
				<label for="Supervisor" class="info-title">{{ trans('client.add_supervisor') }}</label>
				<a href="#" data-toggle="modal" data-target="#add_supervisor" class="addsupervisor">{{ trans('client.add_supervisor') }}</a>
			</div>

			<div class="buttons">
				<button type="submit" class="btn button_primairy">{{ trans('global.save_button') }}</button>
				<button type="button" class="btn button_secondary">{{ trans('global.reset_button') }}</button>
			</div>
		</div>
		{{ Form::close() }}

		<div class="col-md-3 task-section">
			{{ Form::open(['id' => 'search']) }}
				<div class="form-group">
					<input type="search" class="form-control" name="search" placeholder="{{ trans('client.search_task') }}">
				</div>
			{{ Form::close() }}
			<ul id="search-results">

			</ul>
			<ul id="task-list" style="padding:0">
				<li class="task hidden">
					<div class="task-info">
						<div class="time">12:00</div>
						<div class="name">{{ trans('task.task') }}</div>
					</div>
					<div class="task-icons">
						<i class="ion-arrow-down-b"></i>
					</div>
					<div class="options col-md-12">
						<div class="option col-md-3">
							<i class="ion-clock"></i>
							<span>{{ trans('task.time') }}</span>
						</div>
						<div class="option col-md-3">
							<i class="ion-volume-medium volume-icon"></i>
							<span>{{ trans('task.audio') }}</span>
						</div>
						<div class="option col-md-3">
							<i class="ion-printer"></i>
							<span>{{ trans('task.print') }}</span>
						</div>
						<div class="option col-md-3">
							<i class="ion-trash-a delete"></i>
							<span>{{ trans('task.delete') }}</span>
						</div>
					</div>
					<div class="col-md-12 subtasks hidden">
						<ul>

						</ul>
					</div>
					<div class="add-task-button">

						<button data-toggle="modal" data-target="#create_step">{{ trans('step.create') }}</button>
					</div>

					@include('modals.lorrein.step')
				</li>
				<div class="new-task-window">
					<a href="#" data-toggle="modal" data-target="#create_task" class="button-submit task-button">{{ trans('task.create') }}</a>
				</div>
				@if(count($today_tasks) == 0)
					<p class="no-tasks">
						{{ trans('task.notask') }}
					</p>
				@else
					<p class="no-tasks">

					</p>
					@foreach($today_tasks as $task)
						<li class="task">
							<div class="task-info">
								<div class="time">{{ $task->start_time }}</div>
								<div class="name">{{ $task->title }}</div>
							</div>
							<div class="task-icons">
								<i class="ion-arrow-down-b"></i>
							</div>
							<div class="options col-md-12">
								<div class="option col-md-3">
									<i class="ion-clock"></i>
									<span>{{ trans('task.time') }}</span>
								</div>
								<div class="option col-md-3">
									<i class="ion-volume-medium volume-icon"></i>
									<span>{{ trans('task.audio') }}</span>
								</div>
								<div class="option col-md-3">
									<i class="ion-printer"></i>
									<span>{{ trans('task.print') }}</span>
								</div>
								<div class="option col-md-3">
									<i class="ion-trash-a delete"></i>
									<span>{{ trans('task.delete') }}</span>
								</div>
							</div>
							<div class="col-md-12 subtasks hidden">
								<ul>
									@foreach($task->subtask_set as $subtask)
										<div class="icon_img col-xs-1">
											<img src="http://placehold.it/50x50" class="icon">
										</div>
										<li class="subtask col-md-10 col-xs-10"><div class="step">{{$subtask->title}}</div></li>
										<li class="step_preferences col-md-2 col-xs-1"><a data-toggle="modal" data-target="#step_settings"><i class="ion-android-settings"></i></a></li>
										<div style="clear: both"></div>
									@endforeach
								</ul>
							</div>
							<div class="add-task-button">
								<button data-toggle="modal" data-target="#create_step">{{ trans('step.create') }}</button>
							</div>
						</li>
					@endforeach
				@endif
			</ul>
		</div>
		<div class="col-md-6 calendar-section">
			<div id="datetimepicker12"></div>
		</div>
	</div>
</div>

<div id="overlay"></div>

<!-- Lorrein modals -->

@include('modals.lorrein.step_settings')

@include('modals.lorrein.task_settings')

@include('modals.lorrein.delete')

@include('modals.lorrein.secondary_supervisor')

<!-- Create Client Modal -->
@include('modals.create_step')

@include('modals.create_client')

@include('modals.add_supervisor')

<!-- End section -->
@endsection
