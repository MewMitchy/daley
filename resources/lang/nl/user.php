<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lanuage lines are used for the user views.
    | We display text/messages for the user which as settings as 'en'.
    |
    */

   	'name' 			=> 'Naam',
   	'username' 	=> 'Gebruikersnaam',
   	'mail' 			=> 'E-mail',
    'tel'      =>  'Telefoonnummer',
   	'password' 	=> 'Wachtwoord',
   	'language' 	=> 'Taal',
    'timezone'  => 'Tijdzone',
    'timeformat'=> 'Tijdsindeling',

    'settings'  => 'Instellingen',

    'europe'    => 'Europa',
    'vienna'    => 'Wenen',
    'america'   => 'Amerika',
    'caracas'   => 'Caracas'

];
