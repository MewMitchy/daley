<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'            => 'These credentials do not match our records.',
    'throttle'          => 'Too many login attempts. Please try again in :seconds seconds.',
    'username'          => 'Gebruikersnaam',
    'password'          => 'Wachtwoord',
    'no_account'        => "Heeft u geen account?",
    'signup'            => 'Registreer',
    'forgot'            => 'Wachtwoord vergeten?',
    'login'             => 'Inloggen',
    'name'              => 'Naam',
    'email'             => 'E-mail',
    'already_signed_up' => 'Heeft u al een account?',
    'login_here'        => 'Hier inloggen',
    'request_password'  => 'Wachtwoord opvragen'

];