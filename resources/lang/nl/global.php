<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Global Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lanuage lines are used bu multiple views in difference directories so that's why they are global.
    | We display text/messages for the user which as settings as 'nl'.
    |
    */

    'save_button'   => 'Opslaan',
    'cancel_button' => 'Annuleer',
    'reset_button' => 'Reset'


];
