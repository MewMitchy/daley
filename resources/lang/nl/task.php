<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Task Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lanuage lines are used for the task views.
    | We display text/messages for the user which as settings as 'nl'.
    |
    */

    'task'          => 'Taak',
    'time'          => 'Tijd',
    'audio'         => 'Geluid',
    'print'         => 'Uitprinten',
    'delete'        => 'Verwijderen',
   	'name'          => 'Naam',
    'subtask'       => 'Stap',
    'create'        => 'Maak een taak',
    'email'         => 'Email',
    'tel'           => 'Telefoon nummer',
    'secondary'     => 'Voeg secundaire begeleider toe',
    'notask'        => 'Deze cliënt heeft nog geen taken voor vandaag!',
    'title'         => 'Titel',

    // Tasks

    'shower'        => 'Douchen',
    'brush_teeth'   => 'Tanden poetsen',
    'have_breakfast'=> 'Ga ontbijten',
    'have_lunch'    => 'Ga lunchen',
    'have_dinner'   => 'Ga avondeten',
    'go_to_sleep'   => 'Ga slapen'
];
