<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | 
    | 
    |
    */

    'credentials'	            =>	'Uw gegevens zijn onjuist!',
    'required_name'	            =>	'Uw naam is verplicht!',
    'required_email'	        =>	'Uw e-mail is verplicht!',
    'char_username'	            =>	'Uw gebruikersnaam moet langer dan 5 karakters zijn!',
    'char_password'	            =>	'Uw wachtwoord moet langer dan 5 karakters zijn!',
    'register_account_already'	=>	'Dit account is al geregistreerd!',
    'register_account_success'	=>	'Uw account is geregistreerd!',
    'required_name_field'	    =>	'Het naam veld is verplicht!',
    'required_dob_field'	    =>	'Het geboortedatum veld is verplicht!',
    'required_color_field'	    =>	'Het kleur veld is verplicht!',
    'something_went_wrong'	    =>	'Er is iets fout gegaan!',
    'register_client_success'	=>	'Deze cliënt is geregisteerd!',
    'update_failed'	            =>	'Deze cliënt kan niet bijgewerkt worden!',
    'update_success'	        =>	'Deze cliënt is bijgewerkt!',
    'language_success'	        =>	'Uw taalinstellingen zjin gewijzigd!',
    'language_failed'	        =>	'Deze taal wordt niet ondersteund!',
    'task_success'	            =>	"U heeft een taak aangemaakt!",
    'settings_success'	        =>	'Uw instellingen zijn gewijzigd!',
    'delete_client'             =>  'Cliënt verwijderd met het id: ',
    'no_results'                =>  'Geen resultaten',
    'sure_delete'               =>  'Weet u zeker dat u deze taak wilt verwijderen?',
    'no_delete'                 =>  'Nee, verwijder deze taak niet',
    'yes_delete'                =>  'Ja, verwijder deze taak',
    'task_preferences'          =>  'Taak instellingen',
    'step_preferences'          =>  'Stap instellingen',
    'step_description'          =>  'Stap beschrijving',
    'task_description'          =>  'Taak beschrijving',
    'your_step_description'     =>  'Uw stap beschrijving',
    'client_color'              =>  'Kies de kleur voor uw clïent',
    'time'                      =>  'Tijd',
    'minutes'                   =>  'minuten',
    'repeat_task'               =>  'Taak herhalen',
    'daily'                     =>  'Dagelijks',
    'weekly'                    =>  'Wekelijks',
    'monthly'                   =>  'Maandelijks',
    'annually'                  =>  'Jaarlijks',
    'sound'                     =>  'Geluid',
    'print_task'                =>  'Taak uitprinten',
    'timer'                     =>  'Timer',
    'repeat'                    =>  'Herhalen',
    'duplicate'                 =>  'Dupliceren',
    'icon_upload'               =>  'Icoon uploaden',
    'specifications'            =>  'Specificaties',
    'dont_repeat'               =>  "Niet herhalen",
    'your_task_description'     =>  'Uw taak instellingen',
];
