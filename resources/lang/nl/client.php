<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Client Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lanuage lines are used for the client views.
    | We display text/messages for the user which as settings as 'nl'.
    |
    */

    'name' 				=> 'Naam',
    'date_of_birth' 	=> 'Geboortedatum',
    'gender' 			=> 'Geslacht',
    'color_identifier' 	=> 'Kleur',
    'language' 			=> 'Taal',
    'create'            => 'Creëer cliënt',

    'add_client'		=> 'Nieuwe Client',
    'search_task'		=> 'Zoek een taak..',

    'add_supervisor'    => 'Supervisor toevoegen',

    'male'              => 'Man',
    'female'            => 'Vrouw',

    'english'           => 'Engels',
    'dutch'             => 'Nederlands',
    'serbian'           => 'Servisch',

    'panic_call'        => 'Bellen',
    'panic_text'        => 'SMS',
    'panic_mail'        => 'E-mail',
    'panic_button'      => 'Paniek knop',

    'new_client'        => 'Nieuwe cliënt'

];
