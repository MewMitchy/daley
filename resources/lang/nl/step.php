<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Step Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lanuage lines are used for the step views.
    | We display text/messages for the user which as settings as 'nl'.
    |
    */

    'task'		=> 'Taak',
   	'name'      => 'Naam',
    'create'    => 'Stap toevoegen'

];