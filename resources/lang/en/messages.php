<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | 
    | 
    |
    */

    'credentials'	            =>	'Your credentials are incorrect!',
    'required_name'	            =>	'Your name is required!',
    'required_email'	        =>	'Your email is required!',
    'char_username'	            =>	'Your username needs to be longer than 5 characters!',
    'char_password'	            =>	'Your password needs to be longer than 5 characters!',
    'register_account_already'	=>	'This account has already been registered!',
    'register_account_success'	=>	'Your account has been succesfully registered!',
    'required_name_field'	    =>	'The name field is required!',
    'required_dob_field'	    =>	'The date of birth field is required!',
    'required_color_field'	    =>	'The color identifier field is required!',
    'something_went_wrong'	    =>	'Something went wrong!',
    'register_client_success'	=>	'This client has been succesfully registered!',
    'update_failed'	            =>	'This client cannot be updated!',
    'update_success'	        =>	'Client information updated succesfully!',
    'language_success'	        =>	'Your language has been changed!',
    'language_failed'	        =>	'This language is not supported!',
    'task_success'	            =>	"You've succesfully created a task!",
    'settings_success'	        =>	'Your settings has been updated!',
    'delete_client'             =>  'Deleted client with id: ',
    'no_results'                =>  'No results',
    'sure_delete'               =>  'Are you sure you want to delete this task?',
    'no_delete'                 =>  "No, don't delete this task",
    'yes_delete'                =>  'Yes, delete this task',
    'task_preferences'          =>  'Task preferences',
    'step_preferences'          =>  'Step preferences',
    'step_description'          =>  'Step description',
    'task_description'          =>  'Task description',
    'your_step_description'     =>  'Your step description',
    'client_color'              =>  'Choose the color for your client',
    'time'                      =>  'Time',
    'minutes'                   =>  'minutes',
    'repeat_task'               =>  'Repeat task',
    'daily'                     =>  'Daily',
    'weekly'                    =>  'Weekly',
    'monthly'                   =>  'Monthly',
    'annually'                  =>  'Annually',
    'sound'                     =>  'Sound',
    'print_task'                =>  'Print task',
    'timer'                     =>  'Timer',
    'repeat'                    =>  'Repeat',
    'duplicate'                 =>  'Duplicate',
    'icon_upload'               =>  'Upload icon',
    'specifications'            =>  'Specifications',
    'dont_repeat'               =>  "Don't repeat",
    'your_task_description'     =>  'Your task description',
    
];
