<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lanuage lines are used for the user views.
    | We display text/messages for the user which as settings as 'en'.
    |
    */

   	'name' 			=> 'Name',
   	'username' 	=> 'Username',
   	'mail' 			=> 'E-mail',
    'tel'      =>  'Phone number',
   	'password' 	=> 'Password',
   	'language' 	=> 'Language',
    'timezone'  => 'Timezone',
    'timeformat'=> 'Timeformat',

    'settings'  => 'Settings',

    'europe'    => 'Europe',
    'vienna'    => 'Vienna',
    'america'   => 'America',
    'caracas'   => 'Caracas'

];
