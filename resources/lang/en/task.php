<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Task Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lanuage lines are used for the task views.
    | We display text/messages for the user which as settings as 'en'.
    |
    */

    'task'          => 'Task',
    'time'          => 'Time',
    'audio'         => 'Audio',
    'print'         => 'Print',
    'delete'        => 'Delete',
   	'name'          => 'Name',
    'subtask'       => 'Subtask',
    'create'        => 'Create task',
    'email'         => 'Email',
    'tel'           => 'Phone number',
    'secondary'     => 'Add secondairy supervisor',
    'notask'        => 'This client does not have any tasks planned for today!',
    'title'         => 'Title',

    // Tasks

    'shower'        => 'Shower',
    'brush_teeth'   => 'Brush teeth',
    'have_breakfast'=> 'Have breakfast',
    'have_lunch'    => 'Have lunch',
    'have_dinner'   => 'Have dinner',
    'go_to_sleep'   => 'Go to sleep'

];
