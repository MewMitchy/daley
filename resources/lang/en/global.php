<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Global Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lanuage lines are used bu multiple views in difference directories so that's why they are global.
    | We display text/messages for the user which as settings as 'en'.
    |
    */

    'save_button'   => 'Save changes',
    'cancel_button' => 'Cancel',
    'reset_button' => 'Reset'

];
