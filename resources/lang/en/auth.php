<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'            => 'These credentials do not match our records.',
    'throttle'          => 'Too many login attempts. Please try again in :seconds seconds.',
    'username'          => 'Username',
    'password'          => 'Password',
    'no_account'        => "Don't have an account?",
    'signup'            =>  'Sign up',
    'forgot'            => 'Forgot password?',
    'login'             =>  'Login',
    'name'              => 'Name',
    'email'             =>  'Email',
    'already_signed_up' =>  'Already signed up?',
    'login_here'        => 'Login here',
    'request_password'  =>  'Request password'

];
