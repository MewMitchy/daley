<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Step Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lanuage lines are used for the step views.
    | We display text/messages for the user which as settings as 'nl'.
    |
    */

    'task'		=> 'Task',
   	'name'      => 'Name',
    'create'    => 'Add step',
    'settings' => 'Step preferences',
    'description' => 'Step description',
    'time' => 'Time',
    'repeat' => 'Repeat',
    'timer' => 'Timer',
    'sound' => 'Sound',
    'duplicate' => 'Duplicate step',
    'icon' => 'Upload icon'

];