<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Client Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lanuage lines are used for the client views.
    | We display text/messages for the user which as settings as 'en'.
    |
    */

    'name' 				=> 'Name',
    'date_of_birth'		=> 'Date of birth',
    'gender' 			=> 'Gender',
    'color_identifier' 	=> 'Color identifier',
    'language' 			=> 'Language',
    'create'            => 'Create client',

    'add_client'		=> 'Add Client',
    'search_task'		=> 'Search for a task..',

    'add_supervisor'    => 'Add supervisor',

    'male'              => 'Male',
    'female'            => 'Female',

    'english'           => 'English',
    'dutch'             => 'Dutch',
    'serbian'           => 'Serbian',

    'panic_call'        => 'Call',
    'panic_text'        => 'Text',
    'panic_mail'        => 'E-mail',
    'panic_button'      => 'Panic button',

    'new_client'        => 'New client'
];
