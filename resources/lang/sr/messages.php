<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | 
    | 
    |
    */

    'credentials'				=>	'Акредитиве су нетачни!',
    'required_name'				=>	'Ваше име је обавезно!',
    'required_email'			=>	'Ваша емаил је обавезан!',
    'char_username'				=>	'Ваше корисничко име треба да буде дужи од 5 знакова !',
    'char_password'				=>	'Лозинка треба да буде дужи од 5 знакова !',
    'register_account_already'	=>	'Овај налог је већ регистрован!',
    'register_account_success'	=>	'Ваш налог је успешно регистрован!',
    'required_name_field'		=>	'поље име је обавезно !',
    'required_dob_field'		=>	'Датум поља рођења је потребно !',
    'required_color_field'		=>	'поље идентификатор боја је обавезна!',
    'something_went_wrong'		=>	'Нешто није у реду!',
    'register_client_success'	=>	'Овај клијент је успешно регистрован !',
    'update_failed'				=>	'Овај клијент не може бити ажуриран !',
    'update_success'			=>	'информације клијент ажуриран успешно !',
    'language_success'			=>	'Ваш језик је промењен !',
    'language_failed'			=>	'Овај језик није подржана !',
    'task_success'				=>	"успешно сте направили задатак !",
    'settings_success'			=>	'Ваша подешавања је ажуриран !',
    'delete_client'             =>  'Брише клијент са ИД: ',
    'no_results'                =>  'Нема резултата',
    'sure_delete'               =>  'Да ли сте сигурни да желите да избришете овај задатак?',
    'no_delete'                 =>  'Не, немојте брисати овај задатак',
    'yes_delete'                =>  'Да, обрисати овај посао',
    'task_preferences'          =>  'Подешавања Јоб',
    'step_preferences'          =>  'Степ подешавања',
    'step_description'          =>  'Корак опис',
    'task_description'          =>  'zadatak опис',
    'your_step_description'     =>  'Иоур Степ опис',
    'client_color'              =>  'Одаберите боју свог клијента',
    'time'                      =>  'Време',
    'minutes'                   =>  'минута',
    'repeat_task'               =>  'Репеат Задатак',
    'daily'                     =>  'Дневни',
    'weekly'                    =>  'Дневни',
    'monthly'                   =>  'Месечно',
    'annually'                  =>  'Годишњи',
    'sound'                     =>  'Звук',
    'print_task'                =>  'Штампа Посао',
    'timer'                     =>  'Тајмер',
    'repeat'                    =>  'Поновите',
    'duplicate'                 =>  'Дупликат',
    'icon_upload'               =>  'Додај икона',
    'specifications'            =>  'Спецификације',
    'dont_repeat'               =>  "Не понављајте",
    'your_task_description'     =>  'Ваш опис задатак',
    
];
