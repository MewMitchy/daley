<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Client Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lanuage lines are used for the client views.
    | We display text/messages for the user which as settings as 'nl'.
    |
    */

    'name' 				=> 'име',
    'date_of_birth' 	=> 'датум рође',
    'gender' 			=> 'секс',
    'color_identifier' 	=> 'боја',
    'language' 			=> 'језик',
    'create'            => 'створити клијента',

    'add_client'		=> 'нови клијент',
    'search_task'		=> 'Пронаћи посао ..',

    'add_supervisor'    => 'додај супервизор',

    'male'              => 'Мушки',
    'female'            => 'женски',

    'english'           => 'енглески језик',
    'dutch'             => 'холандски',
    'serbian'           => 'Српски',

    'panic_call'        => 'позив',
    'panic_text'        => 'Текстуална порука',
    'panic_mail'        => 'e-маил',
    'panic_button'      => 'Паниц Буттон',

    'new_client'        => 'нови клијент'

];
