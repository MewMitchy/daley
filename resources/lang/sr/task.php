<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Task Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lanuage lines are used for the task views.
    | We display text/messages for the user which as settings as 'nl'.
    |
    */

    'task'          => 'задатак',
    'time'          => 'време',
    'audio'         => 'звук',
    'print'         => 'штампа',
    'delete'        => 'уклонити',
   	'name'          => 'име',
    'subtask'       => 'под задатак',
    'create'        => 'Креирај задатак',
    'email'         => 'емајл',
    'tel'           => 'број телефона',
    'secondary'     => 'Додај супервисор',
    'notask'        => 'Овај клијент нема задатак данас!',
    'title'         => 'наслов',

    // Tasks

    'shower'        => 'туш',
    'brush_teeth'   => 'Оперите зубе',
    'have_breakfast'=> 'Доручковати',
    'have_lunch'    => 'Ручамо',
    'have_dinner'   => 'Вечерати',
    'go_to_sleep'   => 'Ићи на спавање'

];
