<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'            => 'These credentials do not match our records.',
    'throttle'          => 'Too many login attempts. Please try again in :seconds seconds.',
    'username'          => 'корисничко име',
    'password'          => 'лозинка',
    'no_account'        => "Немате налог?",
    'signup'            => 'Регистровати',
    'forgot'            => 'Заборавили сте лозинку?',
    'login'             => 'Пријава',
    'name'              => 'име',
    'email'             => 'емаил',
    'already_signed_up' => 'Да ли имате налог?',
    'login_here'        => 'Пријавите се овде',
    'request_password'  => 'zahtev лозинка'

];