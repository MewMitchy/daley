<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Language Lines
    |--------------------------------------------------------------------------
    |
    | The following lanuage lines are used for the user views.
    | We display text/messages for the user which as settings as 'en'.
    |
    */

   	'name' 			=> 'име',
   	'username' 	=> 'Корисничко име',
   	'mail' 			=> 'емаил',
    'tel'      =>  'број телефона',
   	'password' 	=> 'лозинка',
   	'language' 	=> 'језик',
    'timezone'  => 'Временска зона',
    'timeformat'=> 'Формат времена',

    'settings'  => 'Подешавања',

    'europe'    => 'Европа',
    'vienna'    => 'Беч',
    'america'   => 'Америка',
    'caracas'   => 'Каракас'


];
